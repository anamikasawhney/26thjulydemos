﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26thJulyDemos
{
    internal class MyMethods
    {
        public void GetA()
        {
            Console.WriteLine("This GetA");

        }
        public void GetB()
        {
            Console.WriteLine("This GetB");
        }
    }

    // Creation of Extension Method

    static class ExtendedMethodsToMyMethods
    {

        public static void GetC(this MyMethods myMethods)
        {
            Console.WriteLine("This GetC");

        }
        public static void GetD(this MyMethods myMethods)
        {
            Console.WriteLine("This GetD");

        }
    }
    class Demo
    {
        static void Main()
        {
 MyMethods methods = new MyMethods();
            methods.GetA();
            methods.GetB();
            methods.GetC();
            methods.GetD();

            // Extension Methods > Methods which are added to the original class
            // but without affecing the original class

           
        }
    }
}
