﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26thJulyDemos
{
    internal class NullableDemo
    {
        static void Main()
        {
            
            // By def reference type variables take null
            string name = null;
            name = string.Empty;
            // Value type variables are not nullable
            // int? age = null;
            int? age = 10;
            name = "";
            name = null;
            if(string.IsNullOrEmpty(name))
            {

                Console.WriteLine("Name is empty");
            }
            if(age==null)
            {

            }
            // there is a differnce between null & 0
            // there is a difference between name="" && name=null
            if (age.HasValue)
            {
                Console.WriteLine(age.Value);
            }
            else
                Console.WriteLine("Age conatins null");

            // Take input from user
            byte num;
            //Console.WriteLine("Enter Num");
            //num = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Num");
            byte.TryParse(Console.ReadLine(), out num);
           
            //num = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("Num is " + num);

            

          




        }
    }
}
