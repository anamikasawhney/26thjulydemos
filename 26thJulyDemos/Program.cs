﻿namespace _26thJulyDemos
{
    internal class Program
    {
        static void Main(string[] args)
        {//Errors > 1. Compile Time Error 2. Logical Error 
            // 3. Exception > Errors which might or might not occur
            // Exceptions cant be corrected
            // We can only handle them
            // Exception Handling
            // try catch finally
            // try block will contain the statements that can throw exception
            // catch block > if some exception is thrown by try block
            // catch block will handle them
            // finally block will always be exceuted whether exception
            // comes or not (OPTIONAL)
            // if exception comes > try > catch > finally
            // if there is no exception > try > finally
           
            int x=0;
            byte[] num = new byte[10];
            try
            {
                Console.WriteLine("Enter Value of x");
                x = Byte.Parse(Console.ReadLine());
                
                num[11] = 90;
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {// finally block is used for cleanup activities
                // if we have opened some database connections in try 
                // block and some excpetion comes , it will not ble closed
                // so finally block will be used to close 
                // database connections 
                Console.WriteLine($"Value of x is {x}");
            }
        }


    }
}