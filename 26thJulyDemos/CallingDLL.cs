﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26thJulyDemos
{
    internal class CallingDLL
    {
        static void Main()
        {
            CommonFunctions.NumericFunctions numericFunctions = new CommonFunctions.NumericFunctions();
            Console.WriteLine(numericFunctions.IsEven(10));
            Console.WriteLine(numericFunctions.Add(20,40));
            Console.WriteLine(numericFunctions.PrintTable(8));

        }
    }
}
