﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26thJulyDemos
{
    internal class EHDemo
    {
        static void Main()
        {
            string name = String.Empty;
            int age = 0;
            try
            {
                //age = 10;
               
                Console.WriteLine("Enter Name");
                name = Console.ReadLine();
                if (name.Length < 10)
                    throw new CustomException("Min 10 characrers are needed");
                Console.WriteLine("Enter Age");
                age = Byte.Parse(Console.ReadLine());
                if (age < 20 || age > 30)
                    throw new CustomException("Range for Age is 20 to 30");
            }
           
            catch(CustomException ex)
            {
                name = string.Empty;
                age = 0;
                Console.WriteLine(ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("chatrcaters not allowed");
                //Console.WriteLine(ex.Message);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine("Valu outside range");
                //Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Name is " +  name );
                Console.WriteLine("Age is " + age);
            }
        }
    }
}
