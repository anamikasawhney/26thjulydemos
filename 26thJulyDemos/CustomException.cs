﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26thJulyDemos
{
    internal class CustomException : Exception
    {
        public string Message { get; set; }
        public CustomException(string Message)  : base(Message){
            this.Message = Message;
        }
    }
}
